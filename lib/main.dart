import 'package:flutter/material.dart';
import 'package:randomtest/utils/colors.dart';

void main() {
  runApp(MyTestApp());
}

class MyTestApp extends StatefulWidget {
  @override
  State<MyTestApp> createState() => _MyTestAppState();
}

class _MyTestAppState extends State<MyTestApp> {
  double height = 120;

  double width = 120;

  void changeSize() {
    setState(() {
      height = 750;
      width = 320;
    });
  }

  void normalSize() {
    setState(() {
      height = 180;
      width = 180;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SafeArea(
          child: Scaffold(
            body: Stack(children: [
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                      Color(0xff0f0c29),
                      Color(0xff302b63),
                      Color(0xff24243e)
                    ])),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 8000),
                    curve: Curves.elasticOut,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              Color(0xffaa4b6b),
                              Color(0xffda4453),
                              Color(0xfffcb045)
                            ]),
                        borderRadius: BorderRadius.circular(15)),
                    height: height,
                    width: width,
                    child: GestureDetector(
                      onTap: changeSize,
                      onDoubleTap: normalSize,
                    ),
                  ),
                ),
              ),
            ]),

          ),
        ));
  }
}
